import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarBrand,
  NavItem,
  NavLink,
  NavbarText,
} from "reactstrap";
import { logout } from "../../redux/action/authAction";
import {connect, useDispatch} from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, Redirect, useParams } from "react-router-dom";
import Cookies from 'js-cookie';

const NavbarPage = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const handleSignOut = () => {
    props.dispatch(logout());
    // Cookies.remove('token');
    // localStorage.clear();
  }
  console.log(localStorage.getItem('token'), 'LOCAL');
  // sign out ends


  return (
    <div>
      <Navbar primary color="light" light expand="md">
        <NavbarBrand href="/">reactstrap</NavbarBrand>
        
          <NavLink onClick={handleSignOut} href='/' title="Log Out">
            Log Out
          </NavLink>
       
      </Navbar>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  let actions = bindActionCreators({logout});
  return {
    ...actions, dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (NavbarPage);
